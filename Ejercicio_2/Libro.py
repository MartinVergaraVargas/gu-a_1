#!/usr/bin/env python3

class Libro():

    def __init__(self, titulo, autor, total, prestados):
        self._titulo = titulo
        self._autor = autor
        self._total = total
        self._prestados = prestados

    def get_titulo(self):
        return self.titulo
        
    def get_autor(self):
        return self._autor
        
    def get_total(self):
        return self._total
        
    def get_prestados(self):
        return self._prestados
        


    def set_titulo(self, titulo):
        self._titulo = titulo
        
    def set_autor(self, autor):
        self._autor = autor
        
    def set_total(self, total):
        self._total = total
        
    def set_prestados(self, prestados):
        self._prestados = prestados
        

